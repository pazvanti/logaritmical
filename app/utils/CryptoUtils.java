package utils;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

/**
 * Utility class used for cryptographic functions
 */
public class CryptoUtils {
    private static final int HASH_BYTE_SIZE = 256;
    private static final int PBKDF2_ITERATIONS = 1000;

    private static SecureRandom secureRandom = new SecureRandom();

    /**
     * Generates a cryptographically secure 512 bytes string that can be used as a password salt
     * @return
     */
    public static String generateSalt() {
        byte[] bytes = new byte[512];
        secureRandom.nextBytes(bytes);
        return Base64.getEncoder().encodeToString(bytes);
    }

    /**
     * Generate a hash for a password
     * @param password The password in clear value
     * @param salt A salt used for hashing
     * @return The hashed password
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static String generateHashForPassword(String password, String salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), PBKDF2_ITERATIONS, HASH_BYTE_SIZE);
        byte[] hash = secretKeyFactory.generateSecret(spec).getEncoded();
        return Base64.getEncoder().encodeToString(hash);
    }
}
