package security.auth;

import com.google.inject.Inject;
import data.dto.LoginDTO;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import services.UserService;

import java.util.Optional;

public class UserAuthenticator extends Security.Authenticator {
    @Inject
    private FormFactory formFactory;

    @Inject
    private MessagesApi messagesApi;

    @Override
    public Optional<String> getUsername(Http.Request request) {
        return request.session().get(UserService.USER_NAME_SESSION_KEY);
    }

    @Override
    public Result onUnauthorized(Http.Request request) {
        return unauthorized(views.html.login.render(formFactory.form(LoginDTO.class), messagesApi.preferred(request), request));
    }
}
