package services;

import com.google.inject.Inject;
import data.dao.UserDao;
import data.dao.UserPrivilegeDao;
import data.domain.UserDO;
import data.domain.UserPrivilegeDO;
import data.dto.NewUserDTO;
import data.filter.JPAFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Http;
import utils.CryptoUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static data.domain.UserPrivilegeDO.PRIVILEGE_ALL;

public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public static final String USER_UUID_SESSION_KEY = "userid";
    public static final String USER_NAME_SESSION_KEY = "username";
    public static final String USER_DISPLAY_NAME_SESSION_KEY = "userdisplayname";
    public static final String LOGIN_TIMESTAMP_SESSION_KEY = "login_timestamp";

    @Inject
    private UserDao userDao;

    @Inject
    private UserPrivilegeDao userPrivilegeDao;

    /**
     * Retrieves the currently logged in user by reading the ID from the session.
     * @param request The Http Request
     * @return The logged in user or null if no user was logged in on the current session
     */
    public UserDO findLoggedInUser(Http.Request request) {
        Optional<String> userUUIdFromSession = request.session().get(USER_UUID_SESSION_KEY);
        if (!userUUIdFromSession.isPresent()) {
            return null;
        }

        final String userUUId = userUUIdFromSession.get();
        // TODO: Check if session has expired as well

        JPAFilter userByUUID = new JPAFilter() {
            @Override
            public Predicate getPredicate(CriteriaBuilder criteriaBuilder, Root root) {
                Predicate uuidPredicate = equals(criteriaBuilder, root, "uuid", UUID.fromString(userUUId));
                return uuidPredicate;
            }
        };
        List<UserDO> users = userDao.get(userByUUID);

        if (users == null || users.isEmpty()) return null;

        return users.get(0);
    }

    /**
     * Finds if there is at least one user that has full rights. Needed to prevent some functionality:
     * - deleting last admin
     * - setup page
     * @return If there is at least one active user that has admin privileges
     */
    public boolean isThereAnAdmin(Optional<UUID> exclude) {
        JPAFilter hasAdminPrivilegesFilter = new JPAFilter() {
            @Override
            public Predicate getPredicate(CriteriaBuilder criteriaBuilder, Root root) {
                Predicate userManagementPredicate = equalsIgnoreCase(criteriaBuilder, root, "userManagement", PRIVILEGE_ALL);
                Predicate teamManagementPredicate = equalsIgnoreCase(criteriaBuilder, root, "teamManagement", PRIVILEGE_ALL);
                Predicate logsManagementsPredicate = equalsIgnoreCase(criteriaBuilder, root, "logsManagements", PRIVILEGE_ALL);
                Predicate excludeUserPredicate = null;
                if (exclude.isPresent()) {
                    excludeUserPredicate = notEquals(criteriaBuilder, root, "id", exclude.get());
                }
                return andPredicateBuilder(criteriaBuilder, userManagementPredicate, teamManagementPredicate, logsManagementsPredicate, excludeUserPredicate);
            }
        };

        List<UserPrivilegeDO> privileges = userPrivilegeDao.get(hasAdminPrivilegesFilter);
        return privileges.stream()
                .filter(userPrivilegeDO -> userPrivilegeDO.getUser().getStatus().equals(UserDO.STATUS_ACTIVE))
                .findFirst()
                .isPresent();
    }

    /**
     * Find a user that has the specified username or email address.
     * @param username The username to search for
     * @param email The email to search for
     * @return The first user found that satisfied any of the two conditions
     */
    public UserDO findWithUsernameOrEmail(String username, String email) {
        JPAFilter filter = new JPAFilter() {
            @Override
            public Predicate getPredicate(CriteriaBuilder criteriaBuilder, Root root) {
                Predicate usernamePredicate = equalsIgnoreCase(criteriaBuilder, root, "username", username);
                Predicate emailPredicate = equalsIgnoreCase(criteriaBuilder, root, "email", email);
                return orPredicateBuilder(criteriaBuilder, usernamePredicate, emailPredicate);
            }
        };

        return userDao.get(filter).stream().findFirst().orElse(null);
    }

    /**
     * Register a new User that has Full Admin Privileges
     * @param newUserDTO The new user information
     * @return If the user was created or not
     */
    public boolean registerNewAdminUser(NewUserDTO newUserDTO) {
        UserDO userDO = new UserDO();
        try {
            userDO.setUsername(newUserDTO.getUsername());
            userDO.setEmail(newUserDTO.getEmail());
            userDO.setDisplayName(newUserDTO.getDisplayName());
            userDO.setPasswordSalt(CryptoUtils.generateSalt());
            userDO.setPasswordHash(CryptoUtils.generateHashForPassword(newUserDTO.getPassword(), userDO.getPasswordSalt()));
            userDO.setStatus(UserDO.STATUS_ACTIVE);
            userDO.setUuid(UUID.randomUUID());

            UserPrivilegeDO userPrivilege = new UserPrivilegeDO();
            userPrivilege.setUser(userDO);
            userDO.setPrivilege(userPrivilege);
            userPrivilege.setLogsManagements(PRIVILEGE_ALL);
            userPrivilege.setTeamManagement(PRIVILEGE_ALL);
            userPrivilege.setUserManagement(PRIVILEGE_ALL);

            userDO = userDao.persist(userDO);
            return userDO.getId() != null;
        } catch (Exception e) {
            LOGGER.error("Exception while creating a new user", e);
            return false;
        }
    }

    /**
     * Find the user with the given username and password. The password hash is compared with the one in the DB
     * The password in clear is NOT stored and can't be obtained.
     * @param username The username of the user we are searching for. Can also be the email address.
     * @param password The password in clear. The stored hash is compared with the re-compute of the hash using this password
     * @return The user if it exists and password is valid, null otherwise
     */
    public UserDO findUserByUsernameOrEmailAndPassword(String username, String password) {
        JPAFilter filter = new JPAFilter() {
            @Override
            public Predicate getPredicate(CriteriaBuilder criteriaBuilder, Root root) {
                Predicate usernamePredicate = equalsIgnoreCase(criteriaBuilder, root, "username", username);
                Predicate emailPredicate = equalsIgnoreCase(criteriaBuilder, root, "email", username);
                return criteriaBuilder.or(usernamePredicate, emailPredicate);
            }
        };

        List<UserDO> foundUsers = userDao.get(filter);
        if (foundUsers.isEmpty()) return null;

        UserDO user = foundUsers.get(0);

        try {
            // Recompute the password using the stored salt. Validate that it is equal to the hash that is in the DB
            String recomputedPassword = CryptoUtils.generateHashForPassword(password, user.getPasswordSalt());
            if (recomputedPassword.equals(user.getPasswordHash())) return user;
        } catch (Exception e) {
            LOGGER.error("Error recomputing password for user", e);
        }
        return null;
    }

    /**
     * Update the user in the database
     * @param userDO The User object that needs to be updated
     * @return The updated user
     */
    public UserDO update(UserDO userDO) {
        return userDao.update(userDO);
    }

    /**
     * Change the password of the user. This will generate a new salt, hash the password and store it int he database
     * @param userDO The user for which the password is being changed
     * @param plainTextPassword THe password in plain text
     * @return The updated user
     */
    public UserDO changePassword(UserDO userDO, String plainTextPassword) {
        try {
            userDO.setPasswordSalt(CryptoUtils.generateSalt());
            userDO.setPasswordHash(CryptoUtils.generateHashForPassword(plainTextPassword, userDO.getPasswordSalt()));
            return userDao.update(userDO);
        } catch (Exception e) {
            LOGGER.error("Exception while trying to change the user's password", e);
            return userDO;
        }
    }
}
