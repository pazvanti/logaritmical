package services;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import com.google.inject.Inject;
import data.dao.LogFormatDao;
import data.domain.FormatFieldDO;
import data.domain.LogFormatDO;
import data.dto.FormatFieldDTO;
import data.dto.LogFormatDTO;
import data.dto.LogFormatErrorDTO;

public class LogFormatService {
    @Inject
    private LogFormatDao logFormatDao;

    public LogFormatErrorDTO processRawFormData(Map<String, Object> rawLogData) {
        Map<String, FormatFieldDO> fieldsMap = new HashMap<>();
        String formatName = (String) rawLogData.get("formatName");
        rawLogData.remove("formatName");
        rawLogData.remove("csrfToken");
        rawLogData.entrySet().stream().forEach(rawData -> {
            String inputName = rawData.getKey();
            String tempId = inputName.substring(0, inputName.indexOf('.'));
            FormatFieldDO fieldDO = fieldsMap.get(tempId);
            if (fieldDO == null) {
                fieldDO = new FormatFieldDO();
                fieldsMap.put(tempId, fieldDO);
            }

            String fieldType = inputName.substring(tempId.length() + 1);
            switch (fieldType) {
                case "name":
                    fieldDO.setName((String) rawData.getValue());
                    break;
                case "prefix":
                    fieldDO.setPrefix((String) rawData.getValue());
                    break;
                case "length":
                    String value = (String) rawData.getValue();
                    if (value == null || value.isEmpty()) {
                        value = "0";
                    }
                    fieldDO.setLength(Integer.parseInt(value));
                    break;
                case "suffix":
                    fieldDO.setSuffix((String) rawData.getValue());
                    break;
                case "format":
                    fieldDO.setTypeFormat((String) rawData.getValue());
                    break;
                case "type":
                    fieldDO.setType((String) rawData.getValue());
                    break;
                case "order":
                    fieldDO.setPos(Integer.valueOf((String)rawData.getValue()));
            }
        });

        LogFormatDO logFormatDO = new LogFormatDO();
        logFormatDO.setName(formatName);
        logFormatDO.setFields(fieldsMap.entrySet().stream().map(entry -> entry.getValue()).collect(Collectors.toList()));

        LogFormatErrorDTO error = validate(logFormatDO);
        if (error == null) {
            logFormatDao.persist(logFormatDO);
        }

        return error;
    }

    private LogFormatErrorDTO validate(LogFormatDO logFormatDO) {
        LogFormatErrorDTO error = new LogFormatErrorDTO();
        error.setLogFormat(new LogFormatDTO());
        if (logFormatDO.getName() == null || logFormatDO.getName().isEmpty()) {
            error.addError("name", "No name provided for the format");
        }

        for (FormatFieldDO field:logFormatDO.getFields()) {
            validateField(field, error);
        }

        if (!error.hasErrors()) return null;

        error.getLogFormat().arrangeFields();
        return error;
    }

    private void validateField(FormatFieldDO field, LogFormatErrorDTO error) {
        String tmpId = generateTempId();
        FormatFieldDTO formatFieldDTO = new FormatFieldDTO(tmpId, field);
        error.getLogFormat().addField(formatFieldDTO);

        if (field.getName() == null || field.getName().isEmpty()) {
            error.addError(tmpId, "Invalid name specified");
        }
    }

    private String generateTempId() {
        String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        int maxInt = str.length();
        for (int count = 0; count < 10; count++) {
            builder.append(str.charAt(random.nextInt(maxInt)));
        }

        return builder.toString();
    }
}
