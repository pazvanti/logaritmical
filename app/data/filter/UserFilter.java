package data.filter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class UserFilter extends JPAFilter {
    private String username = null;
    private String status = null;

    /**
     * Set the filter to only retrieve users that have the specified username
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Set the filter to only retrieve users with the given status
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public Predicate getPredicate(CriteriaBuilder criteriaBuilder, Root root) {
        Predicate usernamePredicate = null;
        Predicate statusPredicate = null;
        if (this.username != null) {
            usernamePredicate = equals(criteriaBuilder, root, "username", this.username);
        }
        if (this.status != null) {
            statusPredicate = equals(criteriaBuilder, root, "status", this.status);
        }

        return andPredicateBuilder(criteriaBuilder, usernamePredicate, statusPredicate);
    }
}
