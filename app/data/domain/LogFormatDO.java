package data.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "log_format")
public class LogFormatDO {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @Column
    private String description;

    @OneToMany(fetch = FetchType.EAGER, targetEntity = FormatFieldDO.class, cascade = CascadeType.ALL)
    @OrderBy("pos")
    @JoinColumn(name = "logFormat")
    private List<FormatFieldDO> fields;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FormatFieldDO> getFields() {
        return fields;
    }

    public void setFields(List<FormatFieldDO> fields) {
        this.fields = fields;
        int pos = 0;
        for (FormatFieldDO formatField:fields) {
            formatField.setLogFormat(this);
            formatField.setPos(pos);
            pos++;
        }
    }
}
