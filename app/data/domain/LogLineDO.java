package data.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Table(name = "parsed_logs")
@Entity
public class LogLineDO {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private BigInteger id;

    @Column(name = "uuid")
    // the ID is not UUID because it is computed by appending a UUID to the raw log's ID
    private String uuid;

    @Column(name = "logId", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
    @Type(type = "uuid-char")
    private Integer logId;

    @Column(name = "dateAndTime")
    private String dateAndTime;

    @Column(name = "timestamp")
    private Date timestamp;

    @Column(name = "ndc")
    private String ndc;

    @Column(name = "host")
    private String host;

    @Column(name = "endpoint")
    private String endpoint;

    @Column(name = "application")
    private String application;

    @Column(name = "type")
    private String type;

    @Column(name = "class")
    private String logClass;

    @Column(name = "message")
    private String message;

    @Column(name = "lineNumber")
    private Long lineNumber;

    //TODO: See how we can make this a Map<String, String> that gets mapped to jSon
    @Column(name = "other", columnDefinition = "json")
    private String other;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public String getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(String dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getNdc() {
        return ndc;
    }

    public void setNdc(String ndc) {
        this.ndc = ndc;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLogClass() {
        return logClass;
    }

    public void setLogClass(String logClass) {
        this.logClass = logClass;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }
}
