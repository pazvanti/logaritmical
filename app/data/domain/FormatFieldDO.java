package data.domain;

import javax.persistence.*;

@Entity
@Table(name = "log_format_fields")
public class FormatFieldDO {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column
    private Integer pos;

    @Column
    private String prefix;

    @Column
    private String suffix;

    @Column
    private Integer length;

    @Column
    private String typeFormat;

    @Column
    private String name;

    @Column
    private String type;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "logFormat")
    private LogFormatDO logFormat;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPos() {
        return pos;
    }

    public void setPos(Integer pos) {
        this.pos = pos;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getTypeFormat() {
        return typeFormat;
    }

    public void setTypeFormat(String typeFormat) {
        this.typeFormat = typeFormat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LogFormatDO getLogFormat() {
        return logFormat;
    }

    public void setLogFormat(LogFormatDO logFormat) {
        this.logFormat = logFormat;
    }
}
