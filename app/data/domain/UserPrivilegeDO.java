package data.domain;

import javax.persistence.*;

@Entity
@Table(name = "privileges")
public class UserPrivilegeDO {
    public static final String PRIVILEGE_ALL = "ALL";

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userId")
    @MapsId
    private UserDO user;

    @Column
    private String userManagement;

    @Column
    private String teamManagement;

    @Column
    private String logsManagements;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserDO getUser() {
        return user;
    }

    public void setUser(UserDO user) {
        this.user = user;
    }

    public String getUserManagement() {
        return userManagement;
    }

    public void setUserManagement(String userManagement) {
        this.userManagement = userManagement;
    }

    public String getTeamManagement() {
        return teamManagement;
    }

    public void setTeamManagement(String teamManagement) {
        this.teamManagement = teamManagement;
    }

    public String getLogsManagements() {
        return logsManagements;
    }

    public void setLogsManagements(String logsManagements) {
        this.logsManagements = logsManagements;
    }
}
