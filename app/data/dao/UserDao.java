package data.dao;

import data.domain.UserDO;
import data.filter.UserFilter;

import java.util.List;

public class UserDao extends GenericDao<UserDO> {
    public UserDao() {
        super(UserDO.class);
    }

    public List<UserDO> findActiveByUsername(String username) {
        UserFilter filter = new UserFilter();
        filter.setUsername(username);
        filter.setStatus(UserDO.STATUS_ACTIVE);

        return get(filter);
    }
}
