package data.dao;

import data.domain.UserPrivilegeDO;

public class UserPrivilegeDao extends GenericDao<UserPrivilegeDO> {

    public UserPrivilegeDao() {
        super(UserPrivilegeDO.class);
    }
}
