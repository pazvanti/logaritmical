package data.dao;

import com.google.inject.Inject;
import data.filter.JPAFilter;
import play.db.jpa.JPAApi;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.Collections;
import java.util.List;

/**
 * Generic DAO used for doing JPA operations
 */
public class GenericDao<T> {
    /**
     * The entity manager name. This should be configurable
     */
    private static final String ENTITY_MANAGER_NAME = "default";

    private Class<T> classOfData;

    @Inject
    protected JPAApi jpaApi;

    /**
     * Constructor for a DAO
     * @param classOfData - The class of the domain object that will be persisted and over which JPA operations will be performed
     *                    The class must be an {@link Entity}
     */
    public GenericDao(Class<T> classOfData) {
        if (!classOfData.isAnnotationPresent(Entity.class)) {
            throw new PersistenceException("The domain class must be an Entity!");
        }
        this.classOfData = classOfData;
    }

    /**
     * Returns all items from the database, with the default limit for the maximum number of results
     * @return The entries from the database
     */
    public List<T> getAll() {
        return get(null);
    }

    /**
     * Return the list of item that satisfy the filter provided. If the filter is null, all items will be returned.
     * @param filter The filter for used for returning the items. Can be null, in which case all items are returned
     * @return The list of items that satisfy the given filter
     */
    public List<T> get(JPAFilter filter) {
        EntityManager entityManager = jpaApi.em(ENTITY_MANAGER_NAME);

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(classOfData);
        Root<T> root = criteriaQuery.from(classOfData);
        criteriaQuery.select(root);
        if (filter != null) {
            Predicate predicate = filter.getPredicate(criteriaBuilder, root);
            if (predicate != null) {
                criteriaQuery.where(predicate);
            }
            List<Order> orderList = filter.getOrderBy(criteriaBuilder, root);
            if (!orderList.isEmpty()) {
                criteriaQuery.orderBy(orderList);
            }
        }
        try {
            TypedQuery query = entityManager.createQuery(criteriaQuery);
            if (filter != null) {
                query.setMaxResults(filter.getLimit()).setFirstResult(filter.getOffset());
            } else {
                query.setMaxResults(JPAFilter.DEFAULT_LIMIT).setFirstResult(0);
            }
            List<T> result = query.getResultList();
            return result;
        } catch (NoResultException exception) {
            return Collections.emptyList();
        }
    }

    /**
     * Returns the number of entries that satisfy the given filter
     * @param filter The Filter that will be applied on the results. Can be null
     * @return The number of entries
     */
    public long count(JPAFilter filter) {
        EntityManager entityManager = jpaApi.em(ENTITY_MANAGER_NAME);
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> root = criteriaQuery.from(classOfData);
        criteriaQuery.select(criteriaBuilder.count(root));

        if (filter != null) {
            Predicate predicate = filter.getPredicate(criteriaBuilder, root);
            if (predicate != null) {
                criteriaQuery.where(predicate);
            }
        }

        try {
            TypedQuery<Long> result = entityManager.createQuery(criteriaQuery);
            return result.getSingleResult();
        }  catch (NoResultException exception) {
            return 0l;
        }
    }

    /**
     * Return the entry with the given ID or null if no such entry exists
     * @param id The ID of the object
     * @return The object with the specified ID
     */
    public T find(Object id) {
        EntityManager em = jpaApi.em(ENTITY_MANAGER_NAME);
        return em.find(classOfData, id);
    }

    /**
     * Insert the given object in the database.
     * @param obj The object to store in the database
     * @return The object stored, with the generated values set
     */
    public T persist(T obj) {
        jpaApi.withTransaction(entityManager -> {entityManager.persist(obj);});
        return obj;
    }

    /**
     * Insert the given object in the database. Same as persist();
     * @param obj The object to store in the database
     * @return The object stored, with the generated values set
     */
    public T insert(T obj) {
        return persist(obj);
    }

    /**
     * Update the entry in the database
     * @param obj The object to update
     * @return The updated object
     */
    public T merge(T obj) {
        jpaApi.withTransaction(entityManager -> {entityManager.merge(obj);});
        return obj;
    }

    /**
     * Update the entry in the database. Same as merge();
     * @param obj The object to update
     * @return The updated object
     */
    public T update(T obj) {
        return merge(obj);
    }

    /**
     * Hard deletes the object with the given ID from the database, if it exists
     * @param id The ID of the object that will be deleted
     */
    public void delete(String id) {
        jpaApi.withTransaction(entityManager -> {
            T obj = entityManager.find(classOfData, id);
            if (obj != null) {
                entityManager.remove(obj);
            }
        });
    }

    /**
     * Hard deletes the object from the database
     * @param obj The object that will be deleted
     */
    public void delete(T obj) {
        jpaApi.withTransaction(entityManager -> {
            entityManager.remove(obj);
        });
    }
}
