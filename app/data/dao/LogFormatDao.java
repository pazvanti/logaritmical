package data.dao;

import data.domain.LogFormatDO;

public class LogFormatDao extends GenericDao<LogFormatDO> {
    /**
     * Constructor for a DAO
     */
    public LogFormatDao() {
        super(LogFormatDO.class);
    }
}
