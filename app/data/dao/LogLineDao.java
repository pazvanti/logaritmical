package data.dao;

import data.domain.LogLineDO;

public class LogLineDao extends GenericDao<LogLineDO> {
    /**
     * Constructor for a DAO
     */
    public LogLineDao() {
        super(LogLineDO.class);
    }
}
