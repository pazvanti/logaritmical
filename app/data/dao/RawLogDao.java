package data.dao;

import data.domain.RawLogDO;

public class RawLogDao extends GenericDao<RawLogDO> {
    /**
     * Constructor for a DAO
     */
    public RawLogDao() {
        super(RawLogDO.class);
    }
}
