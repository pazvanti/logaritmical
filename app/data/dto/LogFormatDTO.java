package data.dto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class LogFormatDTO {
    private List<FormatFieldDTO> fields = new ArrayList<>();

    public List<FormatFieldDTO> getFields() {
        return this.fields;
    }

    public void addField(FormatFieldDTO field) {
        this.fields.add(field);
    }

    public void arrangeFields() {
        this.fields = this.fields.stream().sorted(Comparator.comparingInt(FormatFieldDTO::getOrder))
                .collect(Collectors.toList());
    }
}
