package data.dto;

import play.data.validation.Constraints;

public class EditUserDTO {
    @Constraints.Required
    @Constraints.Email
    private String email;

    private String displayName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
