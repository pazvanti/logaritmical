package data.dto;

import play.data.validation.Constraints;

public class PasswordDTO {
    @Constraints.Required
    @Constraints.MinLength(8)
    private String password;

    @Constraints.Required
    private String passwordre;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordre() {
        return passwordre;
    }

    public void setPasswordre(String passwordre) {
        this.passwordre = passwordre;
    }
}
