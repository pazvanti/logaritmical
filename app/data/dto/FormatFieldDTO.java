package data.dto;

import java.util.HashMap;
import java.util.Map;

import data.domain.FormatFieldDO;

public class FormatFieldDTO {
    private String id;
    private int order;
    private Map<String, String> fieldData = new HashMap<>();

    public FormatFieldDTO(String id, FormatFieldDO fieldDO) {
        this.id = id;
        this.order = fieldDO.getPos();
        if (fieldDO.getName() != null) {
            addFieldData("name", fieldDO.getName());
        }
        if (fieldDO.getLength() != null) {
            addFieldData("length", fieldDO.getLength().toString());
        }
        if (fieldDO.getTypeFormat() != null) {
            addFieldData("type", fieldDO.getTypeFormat());
        }
        if (fieldDO.getPos() != null) {
            addFieldData("type", fieldDO.getPos().toString());
        }
        if (fieldDO.getPrefix() != null) {
            addFieldData("type", fieldDO.getPrefix());
        }
        if (fieldDO.getSuffix() != null) {
            addFieldData("type", fieldDO.getSuffix());
        }
    }

    public void addFieldData(String name, String data) {
        this.fieldData.put(name, data);
    }

    public String getId() {
        return this.id;
    }

    public int getOrder() {
        return this.order;
    }

    public Map<String, String> getFieldData() {
        return fieldData;
    }
}
