package data.dto;

import java.util.HashMap;
import java.util.Map;

public class LogFormatErrorDTO {
    private LogFormatDTO logFormat;
    private Map<String, String> errors = new HashMap<>();

    public void addError(String fieldId, String message) {
        errors.put(fieldId, message);
    }

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    public LogFormatDTO getLogFormat() {
        return logFormat;
    }

    public void setLogFormat(LogFormatDTO logFormat) {
        this.logFormat = logFormat;
    }

    public Map<String, String> getErrors() {
        return errors;
    }
}
