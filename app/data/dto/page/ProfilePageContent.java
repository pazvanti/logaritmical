package data.dto.page;

import data.domain.UserDO;
import play.mvc.Http;

public class ProfilePageContent extends PageContent {
    private UserDO user;

    public ProfilePageContent(Http.Request request, UserDO userDO) {
        super(request);
        this.user = userDO;
    }

    public UserDO getUser() {
        return user;
    }

    public void setUser(UserDO user) {
        this.user = user;
    }
}
