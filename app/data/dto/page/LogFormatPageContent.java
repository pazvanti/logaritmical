package data.dto.page;

import data.dto.LogFormatErrorDTO;
import play.mvc.Http;

public class LogFormatPageContent extends PageContent {
    private LogFormatErrorDTO error;

    public LogFormatPageContent(Http.Request request) {
        super(request);
    }

    public LogFormatErrorDTO getError() {
        return error;
    }

    public void setError(LogFormatErrorDTO error) {
        this.error = error;
    }
}
