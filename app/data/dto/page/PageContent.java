package data.dto.page;

import play.mvc.Http;
import services.UserService;

/**
 * Transfer object used for sending needed data to the Twirl templates.
 * Mandatory data (title, Logged in user info, etc.) are in this base class.
 * Depending on the page, additional data can be added by extending this object
 */
public class PageContent {
    private String userDisplayName;
    private String pageTitle;

    public PageContent(Http.Request request) {
        this.userDisplayName = request.session().get(UserService.USER_DISPLAY_NAME_SESSION_KEY).get();
    }

    public String getUserDisplayName() {
        return userDisplayName;
    }

    public void setUserDisplayName(String userDisplayName) {
        this.userDisplayName = userDisplayName;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
}
