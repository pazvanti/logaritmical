CREATE SCHEMA `logaritmical` ;

CREATE TABLE `logaritmical`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(36) NOT NULL,
  `username` TEXT NOT NULL,
  `email` TEXT NOT NULL,
  `passwordHash` TEXT NOT NULL,
  `passwordSalt` TEXT NOT NULL,
  `displayName` TEXT NULL,
  `status` VARCHAR(45) NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
);


CREATE TABLE `logaritmical`.`privileges` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `userId` INT(11) NOT NULL,
  `userManagement` TEXT NULL,
  `teamManagement` TEXT NULL,
  `logsManagements` TEXT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  CONSTRAINT `fk_userId`
      FOREIGN KEY (`userId`)
      REFERENCES `logaritmical`.`users` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE `logaritmical`.`raw_logs` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(36) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `log` TEXT NOT NULL,
  `status` VARCHAR(45) NOT NULL DEFAULT 'NEW',
  `labels` TEXT NULL,
  `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_createdBy_idx` (`createdBy` ASC),
  CONSTRAINT `fk_createdBy`
    FOREIGN KEY (`createdBy`)
    REFERENCES `logaritmical`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE `logaritmical`.`parsed_logs` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(73) NOT NULL,
  `logId` INT(11) NOT NULL,
  `dateAndTime` VARCHAR(24) NULL,
  `timestamp` TIMESTAMP NULL,
  `ndc` VARCHAR(255) NULL,
  `host` VARCHAR(255) NULL,
  `endpoint` VARCHAR(255) NULL,
  `application` VARCHAR(255) NULL,
  `type` VARCHAR(15) NULL,
  `class` VARCHAR(255) NULL,
  `message` TEXT NOT NULL,
  `lineNumber` BIGINT NOT NULL,
  `other` JSON NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  CONSTRAINT `fk_logId`
      FOREIGN KEY (`logId`)
      REFERENCES `logaritmical`.`raw_logs` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE `logaritmical`.`log_format` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
);

CREATE TABLE `logaritmical`.`log_format_fields` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `pos` INT NOT NULL,
  `prefix` VARCHAR(45) NULL,
  `suffix` VARCHAR(45) NULL,
  `length` INT NULL,
  `typeFormat` TEXT NULL,
  `name` VARCHAR(255) NULL,
  `type` VARCHAR(45) NOT NULL DEFAULT 'TEXT',
  `logFormat` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_logFormat_idx` (`logFormat` ASC),
  CONSTRAINT `fk_logFormat`
    FOREIGN KEY (`logFormat`)
    REFERENCES `logaritmical`.`log_format` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

