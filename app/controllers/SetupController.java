package controllers;

import com.google.inject.Inject;
import data.dto.LoginDTO;
import data.dto.NewUserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.UserService;

import java.util.Optional;

public class SetupController extends Controller {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Inject
    private UserService userService;

    @Inject
    private FormFactory formFactory;

    @Inject
    private MessagesApi messagesApi;

    /**
     * Renders the setup view. If there already is an admin we assume that setup has been executed so it will redirect
     * to the login page.
     * @param request The Http Request
     * @return The result that renders the view
     */
    public Result setupView(Http.Request request) {
        boolean isThereAnAdmin = userService.isThereAnAdmin(Optional.empty());
        if (isThereAnAdmin) {
            LOGGER.info("There already is an admin registered. Redirecting to login page.");
            return redirect(routes.LoginController.loginView());
        }
        return ok(views.html.setup.render(formFactory.form(NewUserDTO.class), messagesApi.preferred(request), request));
    }

    /**
     * Performs first time setup by creating a new admin user with the data provided in the {@link Form<NewUserDTO>}
     * Validates the form as well and provides errors if needed.
     * @param request The Http Request
     * @return The result that renders the view
     */
    public Result setup(Http.Request request) {
        LOGGER.info("Entering first time setup and registering an admin");
        Form<NewUserDTO> form = formFactory.form(NewUserDTO.class).bindFromRequest(request);

        if (form.hasErrors()) {
            return ok(views.html.setup.render(form, messagesApi.preferred(request), request));
        } else {
            // TODO: Check how we can do this prior to the first hasError() check
            if (!form.get().getPassword().equals(form.get().getPasswordre())) {
                form = form.withError("passwordMismatch", "Passwords do not match");
            }

            if (userService.findWithUsernameOrEmail(form.get().getUsername(), form.get().getEmail()) != null) {
                form = form.withError("userExists", "A user with the specified username or email already exists");
            }

            if (!userService.registerNewAdminUser(form.get())) {
                form = form.withError("error", "Error creating user. Check logs.");
            }
        }

        if (form.hasErrors()) {
            return ok(views.html.setup.render(form, messagesApi.preferred(request), request));
        }

        LOGGER.info("Registered the first administrator: " + form.get().getUsername());
        return ok(views.html.login.render(formFactory.form(LoginDTO.class), messagesApi.preferred(request), request));
    }
}
