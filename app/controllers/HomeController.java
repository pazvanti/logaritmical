package controllers;

import data.dto.page.PageContent;
import play.mvc.*;
import security.auth.UserAuthenticator;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    @Security.Authenticated(UserAuthenticator.class)
    public Result index(Http.Request request) {
        PageContent pageContent = new PageContent(request);
        pageContent.setPageTitle("Homepage - LOGaritmical");
        return ok(views.html.index.render(pageContent));
    }

}
