package controllers;

import com.google.inject.Inject;
import data.domain.UserDO;
import data.dto.EditUserDTO;
import data.dto.PasswordDTO;
import data.dto.page.ProfilePageContent;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import security.auth.UserAuthenticator;
import services.UserService;

import javax.transaction.Transactional;

import static services.UserService.USER_DISPLAY_NAME_SESSION_KEY;

public class UserController extends Controller {
    @Inject
    private FormFactory formFactory;

    @Inject
    private MessagesApi messagesApi;

    @Inject
    private UserService userService;

    /**
     * Update the details of the current logged in user
     * we assume that it is the current logged in user
     * @param request The HTTP Request
     * @return The result that renders the view
     */
    @Security.Authenticated(UserAuthenticator.class)
    @Transactional
    public Result updateMyDetails(Http.Request request) {
        Form<EditUserDTO> form = formFactory.form(EditUserDTO.class).bindFromRequest(request);
        UserDO user = userService.findLoggedInUser(request);
        if (form.hasErrors()) {
            ProfilePageContent pageContent = new ProfilePageContent(request, user);
            return ok(views.html.profile.render(pageContent, form, formFactory.form(PasswordDTO.class), messagesApi.preferred(request), request));
        }

        user.setDisplayName(form.get().getDisplayName());
        user.setEmail(form.get().getEmail());
        userService.update(user);

        return redirect(routes.UserController.myProfile())
                .addingToSession(request, USER_DISPLAY_NAME_SESSION_KEY, user.getDisplayName());
    }

    @Security.Authenticated(UserAuthenticator.class)
    @Transactional
    public Result changeMyPassword(Http.Request request) {
        Form<PasswordDTO> form = formFactory.form(PasswordDTO.class).bindFromRequest(request);
        UserDO user = userService.findLoggedInUser(request);
        ProfilePageContent pageContent = new ProfilePageContent(request, user);
        if (form.hasErrors()) {
            return ok(views.html.profile.render(pageContent, formFactory.form(EditUserDTO.class), form, messagesApi.preferred(request), request));
        } else {
            // TODO: Check how we can do this prior to the first hasError() check
            if (!form.get().getPassword().equals(form.get().getPasswordre())) {
                form = form.withError("passwordMismatch", "Passwords do not match");
            }
        }

        if (form.hasErrors()) {
            return ok(views.html.profile.render(pageContent, formFactory.form(EditUserDTO.class), form, messagesApi.preferred(request), request));
        }

        userService.changePassword(user, form.get().getPassword());
        return redirect(routes.UserController.myProfile());
    }

    /**
     * Display the My Profile page
     * @param request The HTTP Request
     * @return The result that renders the view
     */
    @Security.Authenticated(UserAuthenticator.class)
    public Result myProfile(Http.Request request) {
        UserDO userDO = userService.findLoggedInUser(request);

        ProfilePageContent pageContent = new ProfilePageContent(request, userDO);
        pageContent.setPageTitle("Edit my profile - LOGaritmical");
        return ok(views.html.profile.render(pageContent, formFactory.form(EditUserDTO.class),
                formFactory.form(PasswordDTO.class), messagesApi.preferred(request), request));
    }
}
