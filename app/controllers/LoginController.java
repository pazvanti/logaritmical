package controllers;

import com.google.inject.Inject;
import data.domain.UserDO;
import data.dto.LoginDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.UserService;

import java.util.Optional;

import static services.UserService.*;

public class LoginController extends Controller {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Inject
    private UserService userService;

    @Inject
    private FormFactory formFactory;

    @Inject
    private MessagesApi messagesApi;

    /**
     * Render the appropriate view for the login page.
     * If there is no admin user saved, we assume that setup has not been executed
     * If there is a user already logged in, we will redirect to the home page (TODO)
     * @param request The Http Request
     * @return The result that renders the view
     */
    public Result loginView(Http.Request request) {
        boolean adminExists = userService.isThereAnAdmin(Optional.empty());
        if (!adminExists) {
            LOGGER.info("There is no admin configured. We assume that first-time setup was not executed. Redirecting to setup page.");
            return redirect(routes.SetupController.setupView());
        }

        UserDO loggedInUser = userService.findLoggedInUser(request);
        if (loggedInUser != null) {
            // The user is already logged in. Redirect to home
            LOGGER.info("User " + loggedInUser.getUsername() + " is already logged in. Redirecting to home page.");
        }
        return ok(views.html.login.render(formFactory.form(LoginDTO.class), messagesApi.preferred(request), request));
    }

    /**
     * Perform the login based on the submitted form. Accepts a {@link Form<LoginDTO>}.
     * Returns unknown user error if the provided username or passwords do not match.
     * Stores the userId and loginTimestamp in the session
     * @param request The Http Request
     * @return The result that renders the view
     */
    public Result login(Http.Request request) {
        Form<LoginDTO> form = formFactory.form(LoginDTO.class).bindFromRequest(request);
        if (form.get() != null) {
            LOGGER.info("Trying to authenticate user with username " + form.get().getUsername());
            UserDO user = userService.findUserByUsernameOrEmailAndPassword(form.get().getUsername(), form.get().getPassword());
            if (user == null) {
                form = form.withError("unknownuser", "Invalid username or password");
            } else {
                // Store user to session
                LOGGER.info("User authentication successful.");
                return redirect(routes.HomeController.index())
                        .addingToSession(request, USER_UUID_SESSION_KEY, user.getUuid().toString())
                        .addingToSession(request, USER_NAME_SESSION_KEY, user.getUsername())
                        .addingToSession(request, USER_DISPLAY_NAME_SESSION_KEY, user.getDisplayName())
                        .addingToSession(request, LOGIN_TIMESTAMP_SESSION_KEY, String.valueOf(System.currentTimeMillis()));
            }
        } else {
            LOGGER.info("Invalid form data provided. Can't authenticate!");
            form = form.withError("unknownuser", "Invalid username or password");
        }
        return ok(views.html.login.render(form, messagesApi.preferred(request), request));
    }

    public Result logout() {
        return redirect(routes.LoginController.loginView()).withNewSession();
    }
}
