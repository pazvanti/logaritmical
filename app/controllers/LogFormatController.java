package controllers;

import com.google.inject.Inject;
import data.dto.LogFormatErrorDTO;
import data.dto.page.LogFormatPageContent;
import data.dto.page.PageContent;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import security.auth.UserAuthenticator;
import services.LogFormatService;

import java.util.Map;

public class LogFormatController extends Controller {
    @Inject
    private FormFactory formFactory;

    @Inject
    private MessagesApi messagesApi;

    @Inject
    private LogFormatService logFormatService;

    /**
     * Display the page for creating a enw log format
     * @param request The HTTP reuquest
     * @return The view of the page
     */
    @Security.Authenticated(UserAuthenticator.class)
    public Result createLogFormat(Http.Request request) {
        LogFormatPageContent pageContent = new LogFormatPageContent(request);
        pageContent.setPageTitle("Create log format - LOGaritmical");
        return ok(views.html.logFormat.render(pageContent, messagesApi.preferred(request), request));
    }

    @Security.Authenticated(UserAuthenticator.class)
    public Result handleLogFormatCreation(Http.Request request) {
        DynamicForm requestData = formFactory.form().bindFromRequest(request);

        LogFormatPageContent pageContent = new LogFormatPageContent(request);
        pageContent.setPageTitle("Create log format - LOGaritmical");
        if (!requestData.value().isPresent()) {
            return ok(views.html.logFormat.render(pageContent, messagesApi.preferred(request), request));
        }
        Map<String, Object> values = requestData.value().get().getData();
        LogFormatErrorDTO error = logFormatService.processRawFormData(values);
        if (error == null) {
            return ok();
        } else {
            pageContent.setError(error);
            return ok(views.html.logFormat.render(pageContent, messagesApi.preferred(request), request));
        }
    }
}
