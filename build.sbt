
name := """LOGaritmical"""
organization := "com.logaritmical"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.4"

libraryDependencies += guice

libraryDependencies ++= Seq(
  javaJpa,
  javaJdbc,
  javaForms,
  "org.hibernate" % "hibernate-entitymanager" % "5.4.24.Final",
  "mysql" % "mysql-connector-java" % "8.0.22",
)