# LOGaritmical

LOGaritmical is an open-source log parser and analysis tool that aims to help viewing, analysis, sharing and colaborating on logs from a wide range of applications. **Currently the project is in early stages and for now does NOT do anything concrete.**

## Status

Early stages - no useful functionality exists

## Desired functionality

Eventually, with more work that will be done in the future, we aim to have the following functionalities:
- User management
- Team management
- Privilege management for users/teams/logs
- Log format configuration (manual, from logcat format)
- Log parsing from text file
- Log parsing from clipboard
- Log viewing after parsing with beautification and highlights of important information
- Log analysis support for annotating/commenting on the logs
- Automatic extraction of useful data based on rules

## For developers

LOGaritmical is written in Java (backend), HTML, CSS, JS, JQuery, Bootstrap (frontend) using the Play Framework as the backbone of the application. 

## Contributing

Even though the project is in early stages, any help is appreciated. It will speed thigns up and get a working version out earlier. If you are interested in contributing to the project, don't hesitate to join us or drop us a message for any suggestions.

## Helper Projects (Special thanks)

[AdminLTE - Bootstrap Admin Template](https://adminlte.io/)

## More information

For more information about the project, major updates and status, visit [Petre Popescu](https://petrepopescu.tech/logaritmical/)

## Licence

The code is currently under GNU General Public License v3.0 https://choosealicense.com/licenses/gpl-3.0/
